/*
 * Copyright (C) 2017 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.borderlands;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.reflect.TypeToken;
import lombok.Value;
import lombok.experimental.NonFinal;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.UUID;

/**
 * Model for the borders.
 * @author Willem Mulder
 */
public @Value class Border {
    /** Whether the UUID was filled during deserialization */
    @NonFinal volatile boolean dirty;

    String name;
    double x, z;
    UUID worldUUID;
    double radius;

    public boolean containsLocation(Location<World> loc) {
        if (loc.getExtent().equals(getWorld())) {
            double diffX = loc.getX() - x;
            double diffZ = loc.getZ() - z;
            return ((int) (diffX * diffX + diffZ * diffZ + .5)) < radius * radius;
        }
        return false;
    }

    public Vector3d getCentrePosition() {
        return new Vector3d(x, 0, z);
    }

    public World getWorld() {
        return Sponge.getServer().getWorld(worldUUID).orElse(null);
    }

    void clearDirty() {
        this.dirty = false;
    }

    public static class Serializer implements TypeSerializer<Border> {
        @Nullable
        @Override
        public Border deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
            String world = value.getNode("world").getString();
            UUID worldUUID;
            try {
                worldUUID = UUID.fromString(world);
            } catch (IllegalArgumentException e) {
                worldUUID = Sponge.getServer().getWorld(world)
                        .orElseThrow(() -> new ObjectMappingException("No such world: " + world))
                        .getUniqueId();
            }

            return new Border(
                    !world.equals(worldUUID.toString()),
                    value.getKey().toString(),
                    value.getNode("location", "x").getDouble(),
                    value.getNode("location", "z").getDouble(),
                    worldUUID,
                    value.getNode("radius").getDouble()
            );
        }

        @Override
        public void serialize(@NonNull TypeToken<?> type, @Nullable Border border, @NonNull ConfigurationNode value) throws ObjectMappingException {
            value.getNode("location", "x").setValue(border.x);
            value.getNode("location", "z").setValue(border.z);
            value.getNode("world").setValue(border.worldUUID.toString());
            value.getNode("radius").setValue(border.radius);
        }
    }
}
