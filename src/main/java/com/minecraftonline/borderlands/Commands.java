/*
 * Copyright (C) 2017 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.borderlands;

import com.google.common.collect.ImmutableMap;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TextElement;
import org.spongepowered.api.text.TextTemplate;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Commands for Borderlands
 * @author Willem Mulder
 */
public class Commands {
    private static final TextTemplate COORDS = TextTemplate.of(
            "(", TextTemplate.arg("x"), ", ", TextTemplate.arg("z"), ")"
    );
    private static final TextTemplate BORDER_FORMAT = TextTemplate.of(
            TextColors.GOLD, TextTemplate.arg("name").color(TextColors.WHITE), ":",
            " centre = ", TextTemplate.arg("centre").color(TextColors.WHITE),
            " radius = ", TextTemplate.arg("radius").color(TextColors.WHITE),
            " world = ", TextTemplate.arg("world").color(TextColors.WHITE)
    );
    private static final Supplier<CommandException> BORDER_NOT_FOUND =
            () -> new CommandException(Text.of(TextColors.RED, "Border not found"));

    private final Borderlands plugin;

    public Commands(Borderlands plugin) {
        this.plugin = plugin;
    }
    
    public void registerCommands() {
        CommandSpec.Builder borderSpecBuilder = CommandSpec.builder();
        borderSpecBuilder.permission("com.minecraftonline.borderlands");
        borderSpecBuilder.description(Text.of("Manage world borders"));

        for (Method m : this.getClass().getDeclaredMethods()) {
            if (m.isAnnotationPresent(Command.class)) {
                Command c = m.getAnnotation(Command.class);

                try {
                    CommandSpec.Builder specBuilder = CommandSpec.builder();
                    specBuilder.description(Text.of(c.description()));
                    specBuilder.permission(c.permissions()[0]);
                    specBuilder.arguments((CommandElement[]) this.getClass().getDeclaredMethod(c.argsSupplier()).invoke(this));
                    specBuilder.executor((src, args) -> {
                        try {
                            return (CommandResult) m.invoke(this, src, args);
                        } catch (ReflectiveOperationException ex) {
                            if (ex.getCause() instanceof CommandException) {
                                throw (CommandException) ex.getCause();
                            }

                            plugin.getLogger().error("Error executing command", ex);
                            throw new CommandException(
                                    Text.of(TextColors.RED, "Uh oh, could not execute your command"),
                                    ex, false);
                        }
                    });

                    borderSpecBuilder.child(specBuilder.build(), c.aliases());
                } catch (ReflectiveOperationException ex) {
                    plugin.getLogger().error("Error registering commands", ex);
                }
            }
        }

        Sponge.getCommandManager().register(plugin, borderSpecBuilder.build(), "border", "bl", "borderlands");
    }

    private CommandElement[] noArguments() {
        return new CommandElement[]{GenericArguments.none()};
    }

    private Supplier<CommandException> missingParameter(String parameter) {
        return () -> new CommandException(Text.of(TextColors.RED, "Missing parameter ", parameter), true);
    }

    @Command(aliases = "info",
            description = "Display Borderlands version information",
            helpLookup = "border.info",
            parent = "border",
            permissions = "com.minecraftonline.borderlands")
    public CommandResult info(CommandSource source, CommandContext context) {
        PluginContainer container = Sponge.getPluginManager().fromInstance(plugin).get();
        source.sendMessage(Text.of(
                TextColors.YELLOW, getClass().getPackage().getImplementationTitle(),
                TextColors.RESET, " version ",
                TextColors.AQUA, getClass().getPackage().getImplementationVersion(),
                TextColors.RESET, " by ",
                TextColors.GREEN, Sponge.getPluginManager().fromInstance(plugin).get().getAuthors()
        ));
        return CommandResult.success();
    }

    @Command(aliases = "list",
            description = "List all borders",
            helpLookup = "border.list",
            parent = "border",
            permissions = "com.minecraftonline.borderlands.list")
    public CommandResult list(CommandSource source, CommandContext context) {
        if (plugin.getBorders().isEmpty()) {
            source.sendMessage(Text.of(
                    TextColors.RED, "There are no registered borders. Forgot to /border reload?"
            ));
        } else {
            source.sendMessage(Text.of(TextColors.GOLD, "Registered borders:"));
            plugin.getBorders().values().forEach((border) -> {
                Location<World> centre = new Location<>(border.getWorld(), border.getCentrePosition());

                source.sendMessage(BORDER_FORMAT, ImmutableMap.<String, TextElement>builder()
                        .put("name", Text.of(border.getName()))
                        .put("centre", COORDS.apply(ImmutableMap.of(
                                "x", Text.of(centre.getX()),
                                "z", Text.of(centre.getZ())
                        )))
                        .put("radius", Text.of(border.getRadius()))
                        .put("world", Text.of(border.getWorld().getName()))
                        .build()
                );
            });
        }
        return CommandResult.success();
    }

    @Command(aliases = "reload",
            description = "Reload borders from the database",
            helpLookup = "border.reload",
            parent = "border",
            permissions = "com.minecraftonline.borderlands.reload")
    public CommandResult reload(CommandSource source, CommandContext context) throws CommandException {
        try {
            int bordersLoaded = plugin.reloadBorders();
            return CommandResult.successCount(bordersLoaded);
        } catch (IOException | ObjectMappingException ex) {
            throw new CommandException(Text.of(TextColors.RED, "Failure while reading borders."), ex, false);
        }
    }


    private CommandElement[] borderName() {
        return new CommandElement[] {
            GenericArguments.choices(
                    Text.of("border"),
                    () -> plugin.getBorders().keySet(),
                    Function.identity()
            )
        };
    }

    @Command(aliases = "remove",
            description = "Remove a border from the database",
            helpLookup = "border.remove",
            parent = "border",
            permissions = "com.minecraftonline.borderlands.remove",
            argsSupplier = "borderName")
    public CommandResult remove(CommandSource source, CommandContext context) throws CommandException {
        try {
            plugin.getBorders().remove(context.<String>getOne("border").orElseThrow(BORDER_NOT_FOUND));
            plugin.saveBorders();
            return CommandResult.success();
        } catch (IOException | ObjectMappingException e) {
            throw new CommandException(Text.of(TextColors.RED, "Failure while writing borders."), e, false);
        }
    }

    public CommandElement[] newBorder() {
        return new CommandElement[] {
            GenericArguments.string(Text.of("name")),
            GenericArguments.doubleNum(Text.of("radius")),
            GenericArguments.location(Text.of("centre"))
        };
    }

    @Command(aliases = {"add", "create"},
            description = "Add a border to the database",
            helpLookup = "border.add",
            parent = "border",
            permissions = "com.minecraftonline.borderlands.add",
            argsSupplier = "newBorder")
    public CommandResult add(CommandSource source, CommandContext context) throws CommandException {
        if (plugin.getBorders().containsKey((String) context.getOne("name").get())) {
            throw new CommandException(Text.of(TextColors.RED, "A border with that name already exists."), false);
        }


        String name = context.<String>getOne("name").orElseThrow(missingParameter("name"));
        double radius = context.<Double>getOne("radius").orElseThrow(missingParameter("radius"));
        Location<World> centre = context.<Location<World>>getOne("centre").orElseThrow(missingParameter("centre"));

        double x = centre.getX();
        double z = centre.getZ();
        UUID worldUUID = centre.getExtent().getUniqueId();

        Border border = new Border(false, name, x, z, worldUUID, radius);
        try {
            plugin.getBorders().put(name, border);
            plugin.saveBorders();
            return CommandResult.success();
        } catch (IOException | ObjectMappingException e) {
            throw new CommandException(Text.of(TextColors.RED, "Failure while writing borders."), e, false);
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    private @interface Command {
        String[] aliases();
        String description();
        String helpLookup() default "";
        String parent() default "";
        String[] permissions();
        String argsSupplier() default "noArguments";
    }
}
