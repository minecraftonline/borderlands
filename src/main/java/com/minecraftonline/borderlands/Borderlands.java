/*
 * Copyright (C) 2017, 2019 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.borderlands;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.val;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializers;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;


@Plugin(id = "borderlands", description = "Add multiple borders to your world")
public class Borderlands {
    @SuppressWarnings("UnstableApiUsage")
    public static final TypeToken<Map<String, Border>> BORDERS_TYPE = new TypeToken<Map<String, Border>>() {};

    @Getter
    private Map<String, Border> borders;

    @Inject
    @Getter
    private Logger logger;

    @Inject
    @ConfigDir(sharedRoot = false)
    private Path configDir;

    @Inject
    @DefaultConfig(sharedRoot = false)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    @Inject
    @DefaultConfig(sharedRoot = false)
    private Path configFile;

    @Getter
    private Settings settings;

    @Listener
    public void onInit(GameInitializationEvent evt) {
        try {
            TypeSerializers.getDefaultSerializers().registerType(TypeToken.of(Border.class), new Border.Serializer());

            Files.createDirectories(configDir);

            this.loadSettings();

            new Commands(this).registerCommands();
            Sponge.getEventManager().registerListeners(this, new PlayerListener(this));
        } catch (IOException | ObjectMappingException e) {
            logger.error("Could not load settings", e);
        }
    }

    @Listener
    public void onGameStartingServer(GameStartingServerEvent event) {
        // Load borders here, worlds are loaded now.
        try {
            this.reloadBorders();
        } catch (IOException | ObjectMappingException e) {
            logger.error("Could not load borders", e);
        }
    }

    @Listener
    public void onReload(GameReloadEvent event) {
        try {
            this.reloadBorders();
        } catch (IOException | ObjectMappingException e) {
            logger.error("Could not load borders", e);
        }
    }

    /**
     * Checks whether a given location is within any {@link Border}.
     * @param loc The {@link Location} to check.
     * @return {@code true} if {@code loc} is within any {@code Border}, or if
     * there are no {@code Border}s for the given {@code loc}'s world,
     * {@code false} otherwise.
     */
    public boolean isWithinAnyBorder(@NonNull Location<World> loc) {
        boolean worldHasBorder = false;
        for (Border border : borders.values()) {
            if (!worldHasBorder && loc.getExtent().equals(border.getWorld())) {
                worldHasBorder = true;
            }

            if (border.containsLocation(loc)) {
                return true;
            }
        }
        return !worldHasBorder;
    }

    /**
     * Checks whether a given transform is within any {@link Border}.
     * Equivalent to calling {@link #isWithinAnyBorder(Location)} with {@code transform.getLocation()}.
     * @param transform The {@link Transform} to check.
     * @return {@code true} if {@code loc} is within any {@code Border}, or if
     * there are no {@code Border}s for the given {@code loc}'s world,
     * {@code false} otherwise.
     */
    public boolean isWithinAnyBorder(@NonNull Transform<World> transform) {
        return isWithinAnyBorder(transform.getLocation());
    }

    /**
     * Returns the border nearest to {@code loc}.
     * @param loc The position to find the nearest border location for.
     * @param inset The optional inset to apply
     * @return A {@link Transform} indicating the nearest position within any
     * border, or {@code null} if there are no borders for {@code loc}'s world.
     */
    public Transform<World> findNearestBorder(final @NonNull Transform<World> loc, double inset) {
        final Vector3d pos = loc.getPosition();
        TreeSet<Transform<World>> transforms = new TreeSet<>(new Comparator<Transform<World>>() {
            // No need to take the root, we're just comparing
            private double horizDistSq(Vector3d p) {
                double diffX = p.getX() - pos.getX();
                double diffZ = p.getZ() - pos.getZ();
                return diffX * diffX + diffZ * diffZ;
            }

            @Override
            public int compare(Transform<World> o1, Transform<World> o2) {
                double diff = horizDistSq(o1.getPosition()) - horizDistSq(o2.getPosition());
                return (int) Math.signum(diff);
            }
        });

        for (Border border : borders.values()) {
            if (!loc.getExtent().equals(border.getWorld())) {
                continue;
            }

            if (border.containsLocation(loc.getLocation())) {
                return loc;
            }

            Vector3d vec = pos.sub(border.getCentrePosition()); // get the vector from the centre to the location
            vec = new Vector3d(vec.getX(), 0, vec.getZ());
            vec = vec.mul((border.getRadius() - inset) / vec.length()); // set the vector's size to the radius
            vec = vec.add(border.getCentrePosition());
            transforms.add(new Transform<>(
                    loc.getExtent(),
                    new Vector3d(vec.getX(), pos.getY(), vec.getZ()),
                    loc.getRotation()
            ));
        }

        if (!transforms.isEmpty() && !isWithinAnyBorder(transforms.first())) {
            throw new AssertionError("Location " + transforms.first() + " isn't within any border!");
        }
        return transforms.isEmpty() ? null : transforms.first();
    }

    /**
     * Reloads the borders from file.
     * @return The number of borders that were loaded.
     * @throws java.io.IOException when an I/O-exception occurs while reading the borders file.
     */
    public int reloadBorders() throws IOException, ObjectMappingException {
        Path bordersPath = configDir.resolve("borders.conf");

        if (!Files.exists(bordersPath)) {
            Files.copy(Borderlands.class.getResourceAsStream("/borders.conf"), bordersPath);
        }

        val loader = HoconConfigurationLoader.builder().setPath(bordersPath).build();
        borders = loader.load().<Map<String, Border>>getValue(BORDERS_TYPE, LinkedHashMap::new);

        if (borders.values().stream().anyMatch(Border::isDirty)) {
            logger.info("Rewriting borders with UUID");
            this.saveBorders();
        }

        logger.info("Loaded {} borders.", borders.size());
        return borders.size();
    }

    /**
     * Saves the current borders to file.
     * @throws IOException when an I/O-exception occurs while writing the borders file.
     */
    public void saveBorders() throws IOException, ObjectMappingException {
        Path borderPath = configDir.resolve("borders.conf");

        val loader = HoconConfigurationLoader.builder().setPath(borderPath).build();
        loader.save(loader.createEmptyNode().setValue(BORDERS_TYPE, borders));

        borders.values().forEach(Border::clearDirty);
    }

    private void loadSettings() throws IOException, ObjectMappingException {
        if (Files.notExists(configFile)) {
            this.configManager.save(this.configManager.createEmptyNode().setValue(Settings.TYPE, new Settings()));
            logger.info("Created default config");
        }

        settings = configManager.load().getValue(Settings.TYPE);
    }

    @ConfigSerializable
    @Data
    public static final class Settings {
        @SuppressWarnings("UnstableApiUsage")
        public static final TypeToken<Settings> TYPE = TypeToken.of(Settings.class);

        @Setting(value="border-message",
                comment="Message players get when they hit the border, see http://minecraft.gamepedia.com/Commands#Raw_JSON_text")
        private Text borderMessage = Text.builder("An unknown force prevents you from going any further")
                .color(TextColors.YELLOW)
                .build();

        @Setting(value="stranded-message",
                comment="Message players get when the plugin can't find a suitable location")
        private Text strandedMessage = Text.builder("You were stranded, and returned to the nearest known location")
                .color(TextColors.RED)
                .build();

        @Setting(value="notification-interval",
                comment="Minimum interval between messages, in milliseconds")
        private long notificationInterval = 1000;
    }
}
